# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: team/gnome <newbyte@postmarketos.org>
pkgname=gnome-builder
pkgver=47.1
pkgrel=0
pkgdesc="Develop software for GNOME"
url="https://wiki.gnome.org/Apps/Builder"
# limited by polkit -> flatpak
# armhf: libpeas2
# ppc64le: libdex
arch="all !armhf !ppc64le !s390x !riscv64"
license="GPL-3.0-or-later"
depends="flatpak-builder py3-lxml py3-gobject3"
makedepends="
	clang-dev
	cmark-dev
	ctags
	desktop-file-utils
	devhelp-dev
	d-spy-dev
	editorconfig-dev
	enchant2-dev
	flatpak-dev
	glib-dev
	gobject-introspection-dev
	gom-dev
	gspell-dev
	gtk4.0-dev
	gtkmm3-dev
	gtksourceview5-dev
	json-glib-dev
	jsonrpc-glib-dev
	libadwaita-dev
	libdazzle-dev
	libdex-dev
	libgit2-glib-dev
	libpanel-dev
	libpeas2-dev
	libportal-dev
	libspelling-dev
	libxml2-dev
	llvm-dev
	meson
	pango-dev
	pcre2-dev
	template-glib-dev
	vala
	vte3-dev
	webkit2gtk-6.0-dev
	"
checkdepends="appstream-glib xvfb-run mesa-dri-gallium"
subpackages="$pkgname-dev $pkgname-lang"
source="
	https://download.gnome.org/sources/gnome-builder/${pkgver%.*}/gnome-builder-$pkgver.tar.xz
	"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dplugin_sysprof=false \
		-Dtracing=false \
		. output
	meson compile -C output
}

check() {
	# Increase the timeout, this can take pretty long, especially on armhf
	dbus-run-session -- \
	xvfb-run -a meson test --print-errorlogs -C output -t 10
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
45690d0a4b7709121bcef0ddf239785c3cef4ae4ace0667b6543650041646ebbf236324867220066c6973367f11b44fab981c30d21ac60e39c40d9d4138930a8  gnome-builder-47.1.tar.xz
"
